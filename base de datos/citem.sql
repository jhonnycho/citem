-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 07-11-2019 a las 00:26:27
-- Versión del servidor: 10.4.6-MariaDB
-- Versión de PHP: 7.1.32

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `citem`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compra`
--

CREATE TABLE `compra` (
  `id` int(11) NOT NULL,
  `factura` varchar(15) NOT NULL,
  `fecha_compra` date NOT NULL,
  `cantidad` int(11) NOT NULL,
  `costo` float NOT NULL,
  `otros_costos` float NOT NULL,
  `detalle` varchar(255) NOT NULL,
  `estado` char(1) DEFAULT NULL,
  `id_empleado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compra`
--

INSERT INTO `compra` (`id`, `factura`, `fecha_compra`, `cantidad`, `costo`, `otros_costos`, `detalle`, `estado`, `id_empleado`) VALUES
(2, '123123', '2019-10-10', 5, 1500, 150, 'impresoras epson termicas', 'A', 1),
(3, '123423141', '2019-10-10', 3, 123, 1, 'sdfsfsdf', 'A', 1),
(4, '442424', '2019-10-10', 4, 123, 11, '21212', 'A', 1),
(5, '1312312', '2019-10-10', 4, 123, 12, 'adasdad', 'A', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `compras_gnv`
--

CREATE TABLE `compras_gnv` (
  `id` int(11) NOT NULL,
  `cantidad_m3` double NOT NULL,
  `precio_m3` double NOT NULL,
  `fecha_compra` date NOT NULL,
  `empleado` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `compras_gnv`
--

INSERT INTO `compras_gnv` (`id`, `cantidad_m3`, `precio_m3`, `fecha_compra`, `empleado`) VALUES
(1, 23, 3, '2019-01-02', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleado`
--

CREATE TABLE `empleado` (
  `id` int(11) NOT NULL,
  `nombres` text NOT NULL,
  `paterno` text DEFAULT NULL,
  `materno` text DEFAULT NULL,
  `ci` text NOT NULL,
  `exp_ci` text DEFAULT NULL,
  `usuario` text NOT NULL,
  `password` varchar(32) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `empleado`
--

INSERT INTO `empleado` (`id`, `nombres`, `paterno`, `materno`, `ci`, `exp_ci`, `usuario`, `password`, `estado`) VALUES
(1, 'Jhonny', 'C', NULL, '123456', 'OR', 'jhonny', '9f311f8b875c76391744309e2060f2e2', 'A'),
(2, 'maria', 'M', 'J', '3321123', 'OR', 'maria', '263bce650e68ab4e23f28263760b9fa5', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `tipo` varchar(255) DEFAULT NULL,
  `marca` varchar(255) NOT NULL,
  `modelo` varchar(255) NOT NULL,
  `numero_serie` varchar(100) DEFAULT NULL,
  `uso` varchar(255) NOT NULL,
  `fecha_registro` date DEFAULT NULL,
  `ultimo_empleado` int(11) DEFAULT NULL,
  `estado` char(1) DEFAULT NULL,
  `sector` varchar(20) NOT NULL,
  `id_sucursal` int(11) DEFAULT NULL,
  `id_compra` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `item`
--

INSERT INTO `item` (`id`, `tipo`, `marca`, `modelo`, `numero_serie`, `uso`, `fecha_registro`, `ultimo_empleado`, `estado`, `sector`, `id_sucursal`, `id_compra`) VALUES
(1, 'COMPUTADORA PORTATIL', 'TOSHIBA', 'CM342', '3213322', 'PARA EL USO DEL GERENTE', '2019-04-09', 1, 'A', 'LIQUIDOS', NULL, NULL),
(2, 'IMPRESORA', 'EPSON', 'LT220', '1323122313', 'FACTURACION', '2019-05-02', 1, 'A', '', NULL, NULL),
(3, 'IMPRESORA', 'EPSON', 'LT220', '1323122313', 'FACTURACION', '2019-05-02', 1, 'A', 'GNV', NULL, NULL),
(4, 'FACTURACIÓN', 'EPSON', 'LT 1234', '12314564', 'en oficinas del gerente', '2019-10-10', 1, 'A', '', NULL, 5),
(5, NULL, '', 'Compra 5', NULL, '', NULL, NULL, NULL, '', NULL, 5),
(6, NULL, '', 'Compra 5', NULL, '', NULL, NULL, NULL, '', NULL, 5),
(7, NULL, '', 'Compra 5', NULL, '', NULL, NULL, NULL, '', NULL, 5);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol`
--

CREATE TABLE `rol` (
  `id` int(11) NOT NULL,
  `rol` varchar(35) NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol`
--

INSERT INTO `rol` (`id`, `rol`, `estado`) VALUES
(1, 'Administrador', 'A'),
(2, 'Encargada', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rol_empleado`
--

CREATE TABLE `rol_empleado` (
  `id_empleado` int(11) NOT NULL,
  `id_rol` int(11) NOT NULL,
  `estado` char(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `rol_empleado`
--

INSERT INTO `rol_empleado` (`id_empleado`, `id_rol`, `estado`) VALUES
(1, 1, 'A'),
(2, 2, 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `sucursal`
--

CREATE TABLE `sucursal` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `direccion` varchar(255) NOT NULL,
  `telefono` varchar(15) NOT NULL,
  `encargado` int(11) NOT NULL,
  `fecha_registro` date NOT NULL,
  `estado` char(1) NOT NULL DEFAULT 'A'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `sucursal`
--

INSERT INTO `sucursal` (`id`, `nombre`, `direccion`, `telefono`, `encargado`, `fecha_registro`, `estado`) VALUES
(1, 'sucursal 01', 'dfkkdkfk', '111111', 1, '2019-04-05', 'A');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ventas_gnv`
--

CREATE TABLE `ventas_gnv` (
  `id` int(11) NOT NULL,
  `cantidad_m3` double NOT NULL,
  `precio_m3` double NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `nit` varchar(15) NOT NULL,
  `fecha` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `empleado` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ventas_gnv`
--

INSERT INTO `ventas_gnv` (`id`, `cantidad_m3`, `precio_m3`, `nombre`, `nit`, `fecha`, `empleado`) VALUES
(1, 45, 4, 'juan', '12323', '2019-01-02 04:00:00', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `compra`
--
ALTER TABLE `compra`
  ADD PRIMARY KEY (`id`),
  ADD KEY `compra_fk` (`id_empleado`);

--
-- Indices de la tabla `compras_gnv`
--
ALTER TABLE `compras_gnv`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `empleado`
--
ALTER TABLE `empleado`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`),
  ADD KEY `item_fk` (`ultimo_empleado`),
  ADD KEY `item_fk_1` (`id_compra`),
  ADD KEY `item_fk_2` (`id_sucursal`);

--
-- Indices de la tabla `rol`
--
ALTER TABLE `rol`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `rol_unique` (`rol`);

--
-- Indices de la tabla `rol_empleado`
--
ALTER TABLE `rol_empleado`
  ADD UNIQUE KEY `rol_empleado_un` (`id_empleado`,`id_rol`),
  ADD KEY `rol_empleado_fk_1` (`id_rol`);

--
-- Indices de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sucursal_fk` (`encargado`);

--
-- Indices de la tabla `ventas_gnv`
--
ALTER TABLE `ventas_gnv`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `compra`
--
ALTER TABLE `compra`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `compras_gnv`
--
ALTER TABLE `compras_gnv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `empleado`
--
ALTER TABLE `empleado`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT de la tabla `rol`
--
ALTER TABLE `rol`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `sucursal`
--
ALTER TABLE `sucursal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `ventas_gnv`
--
ALTER TABLE `ventas_gnv`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `compra`
--
ALTER TABLE `compra`
  ADD CONSTRAINT `compra_fk` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id`);

--
-- Filtros para la tabla `item`
--
ALTER TABLE `item`
  ADD CONSTRAINT `item_fk` FOREIGN KEY (`ultimo_empleado`) REFERENCES `empleado` (`id`),
  ADD CONSTRAINT `item_fk_1` FOREIGN KEY (`id_compra`) REFERENCES `compra` (`id`),
  ADD CONSTRAINT `item_fk_2` FOREIGN KEY (`id_sucursal`) REFERENCES `sucursal` (`id`);

--
-- Filtros para la tabla `rol_empleado`
--
ALTER TABLE `rol_empleado`
  ADD CONSTRAINT `rol_empleado_fk` FOREIGN KEY (`id_empleado`) REFERENCES `empleado` (`id`),
  ADD CONSTRAINT `rol_empleado_fk_1` FOREIGN KEY (`id_rol`) REFERENCES `rol` (`id`);

--
-- Filtros para la tabla `sucursal`
--
ALTER TABLE `sucursal`
  ADD CONSTRAINT `sucursal_fk` FOREIGN KEY (`encargado`) REFERENCES `empleado` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
