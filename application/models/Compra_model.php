<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Compra_model extends CI_Model{
private $tabla = "compra";
private $id = "id";      
            
    function insert($data)
    {
        $this->db->set($data);
        $this->db->insert($this->tabla,$data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data);
    }
    
    function delete($id)
    {   
        $this->db->where('id', $id);
        $this->db->delete($this->tabla);
    }

    function activar($id)
    {   
        $data=array('estado'=>'A');
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }
    
    function get_todos(){
        $query = $this->db->get($this->tabla);
        return $query->result();
    }
}
