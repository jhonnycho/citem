<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Ventas_gnv_model extends CI_Model{
private $tabla = "ventas_gnv";
private $id = "id";      
            
    function insert($data)
    {
        $this->db->set($data);
        $this->db->insert($this->tabla,$data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data);        
    }
    
    function delete($id)
    {   
        $data=array('estado'=>'I');
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }

    function activar($id)
    {   
        $data=array('estado'=>'A');
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }
    
    function get_todos(){
        $query = $this->db->get($this->tabla);
        return $query->result();
    }
}
