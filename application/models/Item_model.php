<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Item_model extends CI_Model{
private $tabla = "item";
private $id = "id";      
            
    function insert($data)
    {
        $this->db->set($data);
        $this->db->insert($this->tabla,$data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data);        
    }
    
    function delete($id)
    {   
        $this->db->where('id', $id);
        $this->db->delete($this->tabla);
    }

    function borrarporcompra($id_compra)
    {   
        $this->db->where('id_compra', $id_compra);
        $this->db->delete($this->tabla);
    }

    function activar($id)
    {   
        $data=array('estado'=>'A');
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }
    
    function get_todos(){
        $query = $this->db->get($this->tabla);
        return $query->result();
    }
    
    function get_equipos_facturacion(){
        $query = $this->db->query("
            select i.marca,i.modelo,i.sector,s.nombre,count(*) as total 
            from item i
            left join sucursal s on (i.id_sucursal = s.id)
            where tipo = 'FACTURACION'
            group by marca,sector,id_sucursal,modelo
            ;
        ");
        return $query->result();
    }

    function get_equipos_mecanico(){
        $query = $this->db->query("
        select i.marca,i.modelo,i.sector,s.nombre,count(*) as total 
        from item i
        left join sucursal s on (i.id_sucursal = s.id)
        where tipo = 'MECANICO'
        group by marca,sector,id_sucursal,modelo
        ;
        ");
        return $query->result();
    }

    function get_equipos_electrico(){
        $query = $this->db->query("
        select i.marca,i.modelo,i.sector,s.nombre,count(*) as total 
        from item i
        left join sucursal s on (i.id_sucursal = s.id)
        where tipo = 'ELECTRICO'
        group by marca,sector,id_sucursal,modelo
        ;
        ");
        return $query->result();
    }
}
