<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Rol_model extends CI_Model{
private $tabla = "rol";
private $id = "id";      
            
    function insert($data)
    {
        $this->db->set($data);
        $this->db->insert($this->tabla,$data);
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data);        
    }
    
    function delete($id)
    {   
        $data=array('estado'=>false);
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }

    function activar($id)
    {   
        $data=array('estado'=>true);
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }
    
    function get_todos(){
        $query = $this->db->get($this->tabla);
        return $query->result();
    }

}
