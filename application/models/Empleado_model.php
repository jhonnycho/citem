<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Empleado_model extends CI_Model{
private $tabla = "empleado";
private $id = "id";      
            
    function insert($data)
    {
        $this->db->set($data);
        $this->db->insert($this->tabla,$data);
        $insert_id = $this->db->insert_id();

        return $insert_id;
    }

    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data);        
    }
    
    function delete($id)
    {   
        $data=array('estado'=>false);
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }

    function activar($id)
    {   
        $data=array('estado'=>true);
        $this->db->where($this->id, $id);
        $this->db->update($this->tabla, $data); 
    }
    
    function get_todos(){
        $query = $this->db->get($this->tabla);
        return $query->result();
    }

    function get($id_cliente)
    {
        $query = $this->db->query('SELECT * FROM cliente WHERE id_cliente = ?',array($id_cliente));
        return $query->row();
    }
    
    function login($usuario,$password)
    {
        $query = $this->db->query('SELECT nombres, paterno, materno, usuario, id FROM empleado WHERE usuario = ? and `password` = md5(?)',array($usuario,$password));
        return $query->row();
    }
}
