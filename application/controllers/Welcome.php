<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	public function login()
	{
		$usuario = $this->input->post('usuario');
		$password = $this->input->post('password');
		$this->load->model('empleado_model');
		$existe=$this->empleado_model->login($usuario,$password);
		print_r($existe);
		if($existe)
		{
			$session = array(
				'nombre'  => $existe->nombres." ".$existe->paterno." ".$existe->materno,
				'id_empleado' => $existe->id,
				'usuario'     => $existe->usuario,
				'logged_in' => TRUE
			);
			
			$this->session->set_userdata($session);
			redirect('welcome/listar', 'location');
		}
		else redirect('welcome/logout', 'location');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		redirect('welcome/index', 'location');
	}

	public function listar()
	{
		if(isset($this->session->logged_in))
		{
			$this->load->model('empleado_model');
			$data['lista']=$this->empleado_model->get_todos();
			//$this->load->view('listar',$data);
			/* $this->template->set('titulo', 'Página de Prueba de la Plantilla');
			$this->template->set('estilos', $this->load->view('formularios/formulario4/index_styles',null,true));
			$this->template->set('scripts', $this->load->view('formularios/formulario4/index_scripts',null,true));
			$this->template->load('plantilla', 'jbody', 'formularios/formulario4/index', $data); */
			$this->template->set('titulo', 'CITEM');
			$this->template->set('estilos', '');
			$this->template->set('scripts', '');
			$this->template->load('plantilla', 'contenido', 'listar', $data);
		}
		else redirect('welcome/logout', 'location');
	}

	public function plantilla()
	{
		$this->load->model('empleado_model');
		$data['lista']=$this->empleado_model->get_todos();
		$this->load->view('plantilla',$data);
	}
}
