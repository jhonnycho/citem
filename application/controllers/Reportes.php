<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reportes extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->load->view('login');
	}

	public function plantilla()
	{
		$this->load->model('empleado_model');
		$data['lista']=$this->empleado_model->get_todos();
		$this->load->view('plantilla',$data);
	}

	public function imprimir()
	{
		$this->load->model('compra_model');
		$data['lista']=$this->compra_model->get_todos();
		$this->load->library('Pdf');
		$this->load->view('view_file',$data);
	}

	public function imprimir_equipos_facturacion()
	{
		$this->load->model('item_model');
		$data['lista']=$this->item_model->get_equipos_facturacion();
		$this->load->library('Pdf');
		$this->load->view('reporte_equipos_facturacion',$data);
	}

	public function imprimir_equipos_mecanicos()
	{
		$this->load->model('item_model');
		$data['lista']=$this->item_model->get_equipos_mecanico();
		$this->load->library('Pdf');
		$this->load->view('reporte_equipos_facturacion',$data);
	}

	public function imprimir_equipos_electricos()
	{
		$this->load->model('item_model');
		$data['lista']=$this->item_model->get_equipos_electrico();
		$this->load->library('Pdf');
		$this->load->view('reporte_equipos_facturacion',$data);
	}

}
