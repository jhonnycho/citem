<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compra extends CI_Controller {

	public function __construct()
    {
		parent::__construct();  
		$this->load->model('compra_model');
		$this->load->model('item_model');
    }

	public function index()
	{
		if(isset($this->session->logged_in))
		{
			$data['lista']=$this->compra_model->get_todos();
			$this->template->set('titulo', 'CITEM');
			$this->template->set('estilos', '');
			$this->template->set('scripts', $this->load->view('inventario/compra/listar_scripts',null,true));
			$this->template->load('plantilla', 'contenido', 'inventario/compra/listar', $data);
		}
		else redirect('welcome/logout', 'location');
	}

	public function nuevo()
	{
		$datos = $this->input->post();
		$datos["id_empleado"]=$this->session->id_empleado;
		$datos["estado"]='A';
		$idcompra = $this->compra_model->insert($datos);
		$this->load->model('item_model');
		echo $datos["cantidad"];
		for($i=1; $i<=$datos["cantidad"];$i++)
		{
			$datositem = array(
				'id_compra' => $idcompra,
				'modelo' => 'Compra '.$idcompra
			);
			$this->item_model->insert($datositem);
		}
	}

	public function editar($id)
	{
		$datos = $this->input->post();
		$this->compra_model->update($id,$datos);
	}

	public function borrar($id)
	{		
		$this->compra_model->delete($id);
		$this->item_model->borrarporcompra($id);
	}

	public function activar($id)
	{		
		$this->compra_model->activar($id);
	}
}
