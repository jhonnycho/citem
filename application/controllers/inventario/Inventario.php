<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inventario extends CI_Controller {

	public function __construct()
    {
		parent::__construct();  
		$this->load->model('inventario_model');		
    }

	public function index()
	{
		if(isset($this->session->logged_in))
		{
			$data['lista']=$this->item_model->get_todos();
			$this->template->set('titulo', 'CITEM');
			$this->template->set('estilos', '');
			$this->template->set('scripts', $this->load->view('administracion/listar_scripts',null,true));
			$this->template->load('plantilla', 'contenido', 'administracion/listar_item', $data);
		}
		else redirect('welcome/logout', 'location');
	}

	public function nuevo()
	{
		$datos = $this->input->post();
		$datos["ultimo_empleado"]=$this->session->id_empleado;
		$datos["estado"]='A';
		$this->item_model->insert($datos);
	}

	public function editar($id)
	{
		$datos = $this->input->post();
		$datos["ultimo_empleado"]=$this->session->id_empleado;
		$datos["estado"]='A';
		$this->item_model->update($id,$datos);
	}

	public function borrar($id)
	{		
		$this->item_model->delete($id);
	}

	public function activar($id)
	{		
		$this->item_model->activar($id);
	}
}
