<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Compras_gnv extends CI_Controller {

	public function __construct()
    {
		parent::__construct();  
		$this->load->model('compras_gnv_model');		
    }

	public function index()
	{
		if(isset($this->session->logged_in))
		{
			$data['lista']=$this->compras_gnv_model->get_todos();
			$this->template->set('titulo', 'CITEM');
			$this->template->set('estilos', '');
			$this->template->set('scripts', $this->load->view('inventario/compras_gnv/listar_scripts',null,true));
			$this->template->load('plantilla', 'contenido', 'inventario/compras_gnv/listar', $data);
		}
		else redirect('welcome/logout', 'location');
	}

	public function nuevo()
	{
		$datos = $this->input->post();
		$this->compras_gnv_model->insert($datos);
	}

	public function editar($id)
	{
		$datos = $this->input->post();
		$this->compras_gnv_model->update($id,$datos);
	}

	public function borrar($id)
	{		
		$this->compras_gnv_model->delete($id);
	}

	public function activar($id)
	{		
		$this->compras_gnv_model->activar($id);
	}
}
