<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sucursal extends CI_Controller {

	public function __construct()
    {
		parent::__construct();  
		$this->load->model('sucursal_model');		
    }

	public function index()
	{
		if(isset($this->session->logged_in))
		{
			$data['lista']=$this->sucursal_model->get_todos();
			$this->template->set('titulo', 'CITEM');
			$this->template->set('estilos', '');
			$this->template->set('scripts', $this->load->view('administracion/sucursal/listar_scripts',null,true));
			$this->template->load('plantilla', 'contenido', 'administracion/sucursal/listar', $data);
		}
		else redirect('welcome/logout', 'location');
	}

	public function nuevo()
	{
		$datos = $this->input->post();
		$this->sucursal_model->insert($datos);
	}

	public function editar($id)
	{
		$datos = $this->input->post();
		$this->sucursal_model->update($id,$datos);
	}

	public function borrar($id)
	{		
		$this->sucursal_model->delete($id);
	}

	public function activar($id)
	{		
		$this->sucursal_model->activar($id);
	}
}
