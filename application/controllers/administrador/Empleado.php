<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Empleado extends CI_Controller {

	public function __construct()
    {
		parent::__construct();  
		$this->load->model('empleado_model');		
		$this->load->model('rol_model');
		$this->load->model('rolempleado_model');
    }

	public function index()
	{
		if(isset($this->session->logged_in))
		{
			$data['lista']=$this->empleado_model->get_todos();
			$data['roles']=$this->rol_model->get_todos();
			$this->template->set('titulo', 'CITEM');
			$this->template->set('estilos', '');
			$this->template->set('scripts', $this->load->view('administracion/empleado/listar_scripts',null,true));
			$this->template->load('plantilla', 'contenido', 'administracion/empleado/listar', $data);
		}
		else redirect('welcome/logout', 'location');
	}

	public function nuevo($id_rol)
	{
		$datos = $this->input->post();
		$datos["password"] = md5($datos["password"]);
		$id_empleado = $this->empleado_model->insert($datos);
		$data = array(
			'id_rol' => $id_rol,
			'id_empleado' => $id_empleado,
			'estado' => 'A'
		);
		$this->rolempleado_model->insert($data);
	}

	public function editar($id)
	{
		$datos = $this->input->post();
		$this->empleado_model->update($id,$datos);
	}

	public function borrar($id)
	{		
		$this->empleado_model->delete($id);
	}

	public function activar($id)
	{		
		$this->empleado_model->activar($id);
	}
}
