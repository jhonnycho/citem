<?php
  $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
  $pdf->SetTitle('My Title');
  $pdf->SetHeaderMargin(30);
  $pdf->SetTopMargin(20);
  $pdf->setFooterMargin(20);
  $pdf->SetAutoPageBreak(true);
  $pdf->SetAuthor('Author');
  $pdf->SetDisplayMode('real', 'default');
  
  $pdf->AddPage('L', 'A4');
  $cabecera = '
  <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#aabcfe;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#669;background-color:#e8edff;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#aabcfe;color:#039;background-color:#b9c9fe;}
.tg .tg-in2i{background-color:#D2E4FC;border-color:#000000;text-align:left;vertical-align:middle}
.tg .tg-0a7q{border-color:#000000;text-align:left;vertical-align:middle}
</style>
<table class="tg">
  <tr>
    <th class="tg-0a7q">DESCRIPCION</th>
    <th class="tg-0a7q">MODELO</th>
    <th class="tg-0a7q">SECTOR<br></th>
    <th class="tg-0a7q">REFERENCIA</th>
    <th class="tg-0a7q">TOTALES</th>
  </tr>
  ';
    $informacion='';
    foreach($lista as $elemento){
      $informacion=$informacion.'
      <tr>
      <td class="tg-in2i">'.$elemento->marca.'</td>
      <td class="tg-in2i">'.$elemento->modelo.'</td>
      <td class="tg-in2i">'.$elemento->sector.'</td>
      <td class="tg-in2i">'.$elemento->nombre.'</td>
      <td class="tg-in2i">'.$elemento->total.'</td>
    </tr>';
    }


    $cola=' 
  </table>
';
    $reporte = $cabecera.$informacion.$cola;
  $pdf->writeHTML($reporte, true, false, true, false, '');
  $pdf->Output('My-File-Name.pdf', 'I');
?>