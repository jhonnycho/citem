<div class="x_title">
  <h2>Lista de Usuario<small>para cada uno de los empleados</small></h2>
  
  <div class="clearfix"></div>
</div>

<div class="x_content">

  <p></p>

  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th>
            <input type="checkbox" id="check-all" class="flat">
          </th>
          <th class="column-title">idPrimaria</th>
          <th class="column-title">nombres</th>
          <th class="column-title">paterno</th>
          <th class="column-title">materno</th>
          <th class="column-title">ci</th>
          <th class="column-title">exp_ci</th>
          <th class="column-title">usuario</th>
          <th class="column-title">password</th>
          <th class="column-title">estado</th>
          <th class="column-title no-link last"><span class="nobr">Action</span>
          </th>
          <th class="bulk-actions" colspan="7">
            <a class="antoo" style="color:#fff; font-weight:500;">Operaciones masivas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
          </th>
        </tr>
      </thead>

      <tbody>
        <?php foreach($lista as $persona): ?>
        <tr class="even pointer">
          <td class="a-center ">
            <input type="checkbox" class="flat" name="table_records">
          </td>
          <td class=" "><?=$persona->id?></td>
          <td class=" "><?=$persona->nombres?></td>
          <td class=" "><?=$persona->paterno?></td>
          <td class=" "><?=$persona->materno?></td>
          <td class=" "><?=$persona->ci?></td>
          <td class=" "><?=$persona->exp_ci?></td>
          <td class=" "><?=$persona->usuario?></td>
          <td class=" "><?=$persona->password?></td>
          <td class=" "><?=$persona->estado?></td>
          <td class=" last"><a href="#">View</a>
          </td>
        </tr>  
<?php endforeach; ?>                        
      </tbody>
    </table>
  </div>


</div>