<?php
  $pdf = new Pdf('L', 'mm', 'A4', true, 'UTF-8', false);
  $pdf->SetTitle('My Title');
  $pdf->SetHeaderMargin(30);
  $pdf->SetTopMargin(20);
  $pdf->setFooterMargin(20);
  $pdf->SetAutoPageBreak(true);
  $pdf->SetAuthor('Author');
  $pdf->SetDisplayMode('real', 'default');
  
  $pdf->AddPage('L', 'A4');
  $cabecera = '
  <style type="text/css">
.tg  {border-collapse:collapse;border-spacing:0;border-color:#bbb;}
.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#594F4F;background-color:#E0FFEB;}
.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#bbb;color:#493F3F;background-color:#9DE0AD;}
.tg .tg-dec7{font-family:"Courier New", Courier, monospace !important;;color:#ffffff;border-color:#000000;text-align:left;vertical-align:top}
.tg .tg-j4gc{font-family:"Courier New", Courier, monospace !important;;background-color:#ffffff;border-color:#000000;text-align:left;vertical-align:top}
.tg .tg-2g2f{font-weight:bold;font-size:18px;font-family:Arial, Helvetica, sans-serif !important;;background-color:#329a9d;color:#ffffff;border-color:#000000;text-align:left;vertical-align:top}
.tg .tg-gusl{font-weight:bold;font-family:Arial, Helvetica, sans-serif !important;;background-color:#329a9d;color:#ffffff;border-color:#000000;text-align:left;vertical-align:top}
.tg .tg-mz8j{font-family:"Courier New", Courier, monospace !important;;background-color:#00d2cb;color:#ffffff;border-color:#000000;text-align:left;vertical-align:top}
.tg .tg-78rm{font-family:"Courier New", Courier, monospace !important;;background-color:#f8ff00;border-color:#000000;text-align:left;vertical-align:top}
</style>
<table class="tg">
  <tr>
    <th class="tg-2g2f" colspan="7">ESTACION DE SERVICIO EBEN-EZER</th>
  </tr>
  <tr>
    <td class="tg-gusl" colspan="7">Informe de Compras</td>
  </tr>
  <tr>
    <td class="tg-mz8j" colspan="4">Nombre:</td>
    <td class="tg-dec7"></td>
    <td class="tg-dec7"></td>
    <td class="tg-dec7"></td>
    
  </tr>
  <tr>
    <td class="tg-mz8j" colspan="4">Departamento:</td>
    <td class="tg-dec7"></td>
    <td class="tg-dec7"></td>
    <td class="tg-dec7"></td>
    
  </tr>
  <tr>
    <td class="tg-mz8j" colspan="4">Puesto:</td>
    <td class="tg-dec7"></td>
    <td class="tg-dec7"></td>
    <td class="tg-dec7"></td>
    
  </tr>
  <tr>
    <td class="tg-78rm">FECHA</td>
    <td class="tg-78rm">PRODUCTO</td>
    
    <td class="tg-78rm">Nº FACTURA</td>
    <td class="tg-78rm">CANTIDAD</td>
    <td class="tg-78rm">COSTO</td>
    <td class="tg-78rm">OTROS</td>
    <td class="tg-78rm">TOTAL</td>
  </tr>
  ';
    $informacion='';
    foreach($lista as $elemento){
      $informacion=$informacion.'
    <tr>
      <td class="tg-j4gc">'.$elemento->fecha_compra.'</td>
      <td class="tg-j4gc">'.$elemento->detalle.'</td>
      <td class="tg-j4gc">'.$elemento->factura.'</td>
      <td class="tg-j4gc">'.$elemento->cantidad.'</td>
      <td class="tg-j4gc">'.$elemento->costo.'</td>
      <td class="tg-j4gc">'.$elemento->otros_costos.'</td>
      <td class="tg-j4gc">'.($elemento->cantidad*$elemento->costo+$elemento->otros_costos).'</td>
      
    </tr>   ';
    }


    $cola=' 
  </table>
';
    $reporte = $cabecera.$informacion.$cola;
  $pdf->writeHTML($reporte, true, false, true, false, '');
  $pdf->Output('My-File-Name.pdf', 'I');
?>