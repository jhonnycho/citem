<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title><?=empty($titulo)?'':$titulo?></title>

    <!-- Bootstrap -->
    <link href="<?php echo base_url('vendors/bootstrap/dist/css/bootstrap.min.css');?>" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url('vendors/font-awesome/css/font-awesome.min.css');?>" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo base_url('vendors/nprogress/nprogress.css');?>" rel="stylesheet">
    <!-- iCheck -->
   <link href="<?php echo base_url('vendors/iCheck/skins/flat/green.css');?>" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo base_url('build/css/custom.min.css');?>" rel="stylesheet">

    <?=empty($estilos)?'':$estilos?>

  </head>

  <body class="nav-md">
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?=site_url('welcome/listar');?>" class="site_title"><i class="fa fa-book"></i> <span>CITEM</span></a>
            </div>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              
              <div class="profile_info">
                <span>Bienvenido,</span>
                <h2><?php echo $this->session->usuario;?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>Administración</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Menú <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="<?php echo site_url('administrador/empleado/index');?>">Usuarios</a></li>
                    <li><a href="<?php echo site_url('administrador/item/index');?>">Items</a></li>
                    </ul>
                  </li>                  
                </ul>
              </div>
              <div class="menu_section">
                <h3>Inventario</h3>
                <ul class="nav side-menu">
                  <li><a><i class="fa fa-home"></i> Menú <span class="fa fa-chevron-down"></span></a>
                    <ul class="nav child_menu">
                    <li><a href="<?php echo site_url('inventario/compra/index');?>">COMPRAS</a></li>
                    <li><a href="<?php echo site_url('administrador/item/index');?>">Items</a></li>
                    <li><a href="<?php echo site_url('inventario/compras_gnv/index');?>">Compras GNV</a></li>
                    <li><a href="<?php echo site_url('inventario/ventas_gnv/index');?>">Ventas GNV</a></li>
                    <li><a target="_blank" href="<?php echo site_url('reportes/imprimir');?>">Reporte de compras</a></li>
                    <li><a target="_blank" href="<?php echo site_url('reportes/imprimir_equipos_facturacion');?>">Reporte de Equipos Facturación</a></li>
                    <li><a target="_blank" href="<?php echo site_url('reportes/imprimir_equipos_mecanicos');?>">Reporte de Equipos Mecánicos</a></li>
                    <li><a target="_blank" href="<?php echo site_url('reportes/imprimir_equipos_electricos');?>">Reporte de Equipos Eléctricos</a></li>
                    </ul>
                  </li>
                </ul>
              </div>
            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              <a data-toggle="tooltip" data-placement="top" title="Settings">
                <span class="glyphicon glyphicon-cog" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="FullScreen">
                <span class="glyphicon glyphicon-fullscreen" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Lock">
                <span class="glyphicon glyphicon-eye-close" aria-hidden="true"></span>
              </a>
              <a data-toggle="tooltip" data-placement="top" title="Logout" href="login.html">
                <span class="glyphicon glyphicon-off" aria-hidden="true"></span>
              </a>
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                  <i class="fa fa-user"></i> <?php echo $this->session->usuario;?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="javascript:;"> Configuración</a></li>                    
                    
                    <li><a href="<?php echo site_url('welcome/logout');?>"><i class="fa fa-sign-out pull-right"></i>Salir</a></li>
                  </ul>
                </li>

                
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">              
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
<?=empty($contenido)?'':$contenido?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->
        

        <!-- footer content -->
        <footer>
          <div class="pull-right">
            CITEM  -  2019
          </div>
          <div class="clearfix"></div>
        </footer>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo base_url('vendors/jquery/dist/jquery.min.js');?>"></script>
    <!-- Bootstrap -->
    <script src="<?php echo base_url('vendors/bootstrap/dist/js/bootstrap.min.js');?>"></script>
    <!-- FastClick -->
    <script src="<?php echo base_url('vendors/fastclick/lib/fastclick.js');?>"></script>
    <!-- NProgress -->
    <script src="<?php echo base_url('vendors/nprogress/nprogress.js');?>"></script>
    <!-- iCheck -->
    <script src="<?php echo base_url('vendors/iCheck/icheck.min.js');?>"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo base_url('build/js/custom.min.js');?>"></script>

    <?=empty($scripts)?'':$scripts?>
  </body>
</html>
