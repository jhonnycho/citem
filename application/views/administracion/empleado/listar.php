<div class="x_title">
  <h2>Lista de Empleados<small></small></h2>

  <div class="clearfix">
    <button type="button" class="btn btn-primary" onclick="nuevo();">Nuevo</button>
  </div>
</div>

<div class="x_content">

  <p></p>

  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th>
            <input type="checkbox" id="check-all" class="flat">
          </th>
          <th class="column-title">idPrimaria</th>
          <th class="column-title">nombres</th>
          <th class="column-title">paterno</th>
          <th class="column-title">materno</th>
          <th class="column-title">ci</th>
          <th class="column-title">exp_ci</th>
          <th class="column-title">usuario</th>
          <th class="column-title">password</th>
          <th class="column-title">estado</th>
          <th class="column-title no-link last"><span class="nobr">Action</span>
          </th>
          <th class="bulk-actions" colspan="7">
            <a class="antoo" style="color:#fff; font-weight:500;">Operaciones masivas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
          </th>
        </tr>
      </thead>

      <tbody>
        <?php foreach ($lista as $empleado) : ?>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" "><?= $empleado->id ?></td>
            <td class=" "><?= $empleado->nombres ?></td>
            <td class=" "><?= $empleado->paterno ?></td>
            <td class=" "><?= $empleado->materno ?></td>
            <td class=" "><?= $empleado->ci ?></td>
            <td class=" "><?= $empleado->exp_ci ?></td>
            <td class=" "><?= $empleado->usuario ?></td>
            <td class=" "><?= $empleado->password ?></td>
            <td class=" "><?= $empleado->estado ?></td>
            <td class=" last">
              <button type="button" class="btn btn-sm btn-primary" onclick="editar(<?=$empleado->id?>);"><i class="fa fa-edit"></i></button>
              <button type="button" class="btn btn-sm btn-danger" onclick="borrar(<?=$empleado->id?>);"><i class="fa fa-trash"></i></button>
              <button type="button" class="btn btn-sm btn-warning" onclick="activar(<?=$empleado->id?>);"><i class="fa fa-rotate-left"></i></button>
            </td>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>


<!-- modals -->
<!-- Large modal -->


<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Empleado</h4>
      </div>
      <div class="modal-body">
        <form id="formulario" action="#">
        <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="nombres">nombres<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="nombres" id="nombres" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="paterno">paterno<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="paterno" id="paterno" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="materno">materno<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="materno" id="materno" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="ci">ci<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="ci" id="ci" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="exp_ci">exp_ci<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="exp_ci" id="exp_ci" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="usuario">usuario<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="usuario" id="usuario" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="password">password<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="password" id="password" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="id_rol">Rol<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select id="id_rol"  class="form-control col-md-7 col-xs-12">
                          <option value=""></option>
                            <?php foreach($roles as $rol): ?>
                            <option value="<?=$rol->id?>"><?=$rol->rol?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                      </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="estado">estado<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="estado" id="estado" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="guardar-btn" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">¿Borrar?</h4>
      </div>
      <div class="modal-body">
        Esta seguro de Borrar el Bloque
      </div>
      <div class="modal-footer">
        <button id="confirmar-guardar-btn" type="button" class="btn btn-primary">Si</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<script>
js_data = '<?php echo json_encode($lista);?>';
js_obj_data = JSON.parse(js_data);
</script>