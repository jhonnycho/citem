<div class="x_title">
  <h2>Lista de Item's<small></small></h2>
  
  <div class="clearfix">
  <button type="button" class="btn btn-primary" onclick="nuevo();">Nuevo</button>
  </div>
</div>

<div class="x_content">

  <p></p>

  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th>
            <input type="checkbox" id="check-all" class="flat">
          </th>
          <th class="column-title">id</th>
          <th class="column-title">tipo</th>
          <th class="column-title">marca</th>
          <th class="column-title">modelo</th>
          <th class="column-title">numero_serie</th>
          <th class="column-title">uso</th>
          <th class="column-title">fecha_registro</th>
          <th class="column-title">sector</th>
          <th class="column-title">estado</th>
          <th class="column-title no-link last"><span class="nobr">Operación</span>
          </th>
          <th class="bulk-actions" colspan="7">
            <a class="antoo" style="color:#fff; font-weight:500;">Operaciones masivas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
          </th>
        </tr>
      </thead>

      <tbody>
        <?php foreach($lista as $item): ?>
        <tr class="even pointer">
          <td class="a-center ">
            <input type="checkbox" class="flat" name="table_records">
          </td>
          <td class=" "><?=$item->id?></td>
          <td class=" "><?=$item->tipo?></td>
          <td class=" "><?=$item->marca?></td>
          <td class=" "><?=$item->modelo?></td>
          <td class=" "><?=$item->numero_serie?></td>
          <td class=" "><?=$item->uso?></td>
          <td class=" "><?=$item->fecha_registro?></td>
          <td class=" "><?=$item->sector?></td>
          <td class=" "><?=$item->estado?></td>
          <td class=" last">
            <button type="button" class="btn btn-sm btn-primary" onclick="editar(<?=$item->id?>);"><i class="fa fa-edit"></i></button>
            <button type="button" class="btn btn-sm btn-danger" onclick="borrar(<?=$item->id?>);"><i class="fa fa-trash"></i></button>
            <button type="button" class="btn btn-sm btn-warning" onclick="activar(<?=$item->id?>);"><i class="fa fa-rotate-left"></i></button>
          </td>
        </tr>  
<?php endforeach; ?>                        
      </tbody>
    </table>
  </div>
</div>


<!-- modals -->
<!-- Large modal -->


<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Item</h4>
      </div>
      <div class="modal-body">
        <form id="formulario" action="#">
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="tipo">Tipo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="tipo" id="tipo"  class="form-control col-md-7 col-xs-12">
                            <option value="FACTURACION">FACTURACION</option>
                            <option value="MECANICO">MECANICO</option>
                            <option value="ELECTRICO">ELECTRICO</option>
                          </select>
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="marca">Marca<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="marca" id="marca" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="modelo">Modelo<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="modelo" id="modelo" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="numero_serie">Número de Serie<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="numero_serie" id="numero_serie" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="uso">Uso<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="uso" id="uso" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="fecha_registro">Fecha de Registro<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="date" name="fecha_registro" id="fecha_registro" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="sector">sector<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="sector" id="sector"  class="form-control col-md-7 col-xs-12">
                          <option value="LIQUIDOS">LIQUIDOS</option>
                          <option value="GNV">GNV</option>
                          </select>
                        </div>
                      </div>
                      <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="id_sucursal">Sucursal<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <select name="id_sucursal" id="id_sucursal"  class="form-control col-md-7 col-xs-12">
                            <?php foreach($sucursales as $sucursal): ?>
                            <option value="<?=$sucursal->id?>"><?=$sucursal->nombre?></option>
                            <?php endforeach;?>
                          </select>
                        </div>
                      </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="guardar-btn" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmar" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">¿Borrar?</h4>
        </div>
        <div class="modal-body">
          Esta seguro de Borrar el Bloque
        </div>
        <div class="modal-footer">
          <button id="confirmar-guardar-btn" type="button" class="btn btn-primary" >Si</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

<script>
var js_data = '<?php echo json_encode($lista);?>';
var js_obj_data = JSON.parse(js_data);
</script>
<style>
  label{text-align: right;}
</style>