<div class="x_title">
  <h2>Lista de Sucursales<small></small></h2>
  
  <div class="clearfix">
  <button type="button" class="btn btn-primary" onclick="nuevo();">Nuevo</button>
  </div>
</div>

<div class="x_content">

  <p></p>

  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th>
            <input type="checkbox" id="check-all" class="flat">
          </th>
          <th class="column-title">nombre</th>
          <th class="column-title">direccion</th>
          <th class="column-title">telefono</th>
          <th class="column-title">encargado</th>
          <th class="column-title">fecha_registro</th>
          <th class="column-title">estado</th>
          <th class="column-title no-link last"><span class="nobr">Operación</span>
          </th>
          <th class="bulk-actions" colspan="7">
            <a class="antoo" style="color:#fff; font-weight:500;">Operaciones masivas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
          </th>
        </tr>
      </thead>

      <tbody>
        <?php foreach($lista as $sucursal): ?>
        <tr class="even pointer">
          <td class="a-center ">
            <input type="checkbox" class="flat" name="table_records">
          </td>
          <td class=" "><?=$sucursal->nombre?></td>
          <td class=" "><?=$sucursal->direccion?></td>
          <td class=" "><?=$sucursal->telefono?></td>
          <td class=" "><?=$sucursal->encargado?></td>
          <td class=" "><?=$sucursal->fecha_registro?></td>
          <td class=" "><?=$sucursal->estado?></td>
          <td class=" last">
            <button type="button" class="btn btn-sm btn-primary" onclick="editar(<?=$sucursal->id?>);"><i class="fa fa-edit"></i></button>
            <button type="button" class="btn btn-sm btn-danger" onclick="borrar(<?=$sucursal->id?>);"><i class="fa fa-trash"></i></button>
            <button type="button" class="btn btn-sm btn-warning" onclick="activar(<?=$sucursal->id?>);"><i class="fa fa-rotate-left"></i></button>
          </td>
        </tr>  
<?php endforeach; ?>                        
      </tbody>
    </table>
  </div>
</div>


<!-- modals -->
<!-- Large modal -->


<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Nueva Sucursal</h4>
      </div>
      <div class="modal-body">
        <form id="formulario" action="#">
        <div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="nombre">nombre<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="nombre" id="nombre" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="direccion">direccion<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="direccion" id="direccion" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="telefono">telefono<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="telefono" id="telefono" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="encargado">encargado<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="encargado" id="encargado" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="fecha_registro">fecha_registro<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="fecha_registro" id="fecha_registro" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
<div class="form-group">
                        <label class="control-label col-md-6 col-sm-6 col-xs-12" for="estado">estado<span class="required">*</span>
                        </label>
                        <div class="col-md-6 col-sm-6 col-xs-12">
                          <input type="text" name="estado" id="estado" required="required" class="form-control col-md-7 col-xs-12">
                        </div>
                      </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="guardar-btn" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmar" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">¿Borrar?</h4>
        </div>
        <div class="modal-body">
          Esta seguro de Borrar el Bloque
        </div>
        <div class="modal-footer">
          <button id="confirmar-guardar-btn" type="button" class="btn btn-primary" >Si</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

<script>
var js_data = '<?php echo json_encode($lista);?>';
var js_obj_data = JSON.parse(js_data);
</script>