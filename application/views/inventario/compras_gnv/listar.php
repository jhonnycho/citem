<div class="x_title">
  <h2>Lista de Compras GNV<small></small></h2>

  <div class="clearfix">
    <button type="button" class="btn btn-primary" onclick="nuevo();">Nuevo</button>
  </div>
</div>

<div class="x_content">

  <p></p>

  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th>
            <input type="checkbox" id="check-all" class="flat">
          </th>
          <th class="column-title">cantidad_m3</th>
          <th class="column-title">precio_m3</th>
          <th class="column-title">fecha_compra</th>
          <th class="column-title">empleado</th>
          <th class="column-title no-link last"><span class="nobr">Action</span>
          </th>
          <th class="bulk-actions" colspan="7">
            <a class="antoo" style="color:#fff; font-weight:500;">Operaciones masivas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
          </th>
        </tr>
      </thead>

      <tbody>
        <?php foreach ($lista as $compra_gnv) : ?>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" "><?= $compra_gnv->cantidad_m3 ?></td>
            <td class=" "><?= $compra_gnv->precio_m3 ?></td>
            <td class=" "><?= $compra_gnv->fecha_compra ?></td>
            <td class=" "><?= $compra_gnv->empleado ?></td>
            <td class=" last">
              <button type="button" class="btn btn-sm btn-primary" onclick="editar(<?=$compra_gnv->id?>);"><i class="fa fa-edit"></i></button>
              <button type="button" class="btn btn-sm btn-danger" onclick="borrar(<?=$compra_gnv->id?>);"><i class="fa fa-trash"></i></button>
              <button type="button" class="btn btn-sm btn-warning" onclick="activar(<?=$compra_gnv->id?>);"><i class="fa fa-rotate-left"></i></button>
            </td>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>


<!-- modals -->
<!-- Large modal -->


<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Empleado</h4>
      </div>
      <div class="modal-body">
        <form id="formulario" action="#">
        <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="cantidad_m3">cantidad_m3<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="cantidad_m3" id="cantidad_m3" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="precio_m3">precio_m3<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="precio_m3" id="precio_m3" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="fecha_compra">fecha_compra<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="fecha_compra" id="fecha_compra" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="empleado">empleado<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="empleado" id="empleado" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="guardar-btn" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">¿Borrar?</h4>
      </div>
      <div class="modal-body">
        Esta seguro de Borrar el elemento
      </div>
      <div class="modal-footer">
        <button id="confirmar-guardar-btn" type="button" class="btn btn-primary">Si</button>
        <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
      </div>
    </div>
  </div>
</div>

<script>
js_data = '<?php echo json_encode($lista);?>';
js_obj_data = JSON.parse(js_data);
</script>