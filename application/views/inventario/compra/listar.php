<div class="x_title">
  <h2>Lista de Compras<small></small></h2>

  <div class="clearfix">
    <button type="button" class="btn btn-primary" onclick="nuevo();">Nuevo</button>
  </div>
</div>

<div class="x_content">

  <p></p>

  <div class="table-responsive">
    <table class="table table-striped jambo_table bulk_action">
      <thead>
        <tr class="headings">
          <th>
            <input type="checkbox" id="check-all" class="flat">
          </th>
          <th class="column-title">Id</th>
          <th class="column-title">Factura</th>
          <th class="column-title">Fecha de compra</th>          
          <th class="column-title">Cantidad</th>
          <th class="column-title">Costo</th>
          <th class="column-title">Otros Costos</th>
          <th class="column-title">Detalle</th>
          <th class="column-title">Estado</th>
          <th class="column-title">id_empleado</th>
          <th class="column-title no-link last"><span class="nobr">Action</span>
          </th>
          <th class="bulk-actions" colspan="7">
            <a class="antoo" style="color:#fff; font-weight:500;">Operaciones masivas ( <span class="action-cnt"> </span> ) <i class="fa fa-chevron-down"></i></a>
          </th>
        </tr>
      </thead>

      <tbody>
        <?php foreach ($lista as $compra) : ?>
          <tr class="even pointer">
            <td class="a-center ">
              <input type="checkbox" class="flat" name="table_records">
            </td>
            <td class=" "><?= $compra->id ?></td>
            <td class=" "><?= $compra->factura ?></td>
            <td class=" "><?= $compra->fecha_compra ?></td>            
            <td class=" "><?= $compra->cantidad ?></td>
            <td class=" "><?= $compra->costo ?></td>
            <td class=" "><?= $compra->otros_costos ?></td>
            <td class=" "><?= $compra->detalle ?></td>
            <td class=" "><?= $compra->estado ?></td>
            <td class=" "><?= $compra->id_empleado ?></td>
            <td class=" last">
              <button type="button" class="btn btn-sm btn-primary" onclick="editar(<?=$compra->id?>);"><i class="fa fa-edit"></i></button>
              <button type="button" class="btn btn-sm btn-danger" onclick="borrar(<?=$compra->id?>);"><i class="fa fa-trash"></i></button>
              <button type="button" class="btn btn-sm btn-warning" onclick="activar(<?=$compra->id?>);"><i class="fa fa-rotate-left"></i></button>
            </td>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>


<!-- modals -->
<!-- Large modal -->


<div id="modal" class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">

      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
        </button>
        <h4 class="modal-title" id="myModalLabel">Nuevo Empleado</h4>
      </div>
      <div class="modal-body">
        <form id="formulario" action="#">          
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="factura">Número de Factura<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="text" name="factura" id="factura" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="fecha_compra">Fecha de Compra<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="date" name="fecha_compra" id="fecha_compra" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>          
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="cantidad">Cantidad<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="number" name="cantidad" id="cantidad" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="costo">Costo<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="number" name="costo" id="costo" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="otros_costos">Otros Costos<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <input type="number" name="otros_costos" id="otros_costos" required="required" class="form-control col-md-7 col-xs-12">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-6 col-sm-6 col-xs-12" for="detalle">Detalle<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <textarea name="detalle" id="detalle" required="required" class="form-control col-md-7 col-xs-12"></textarea
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button id="guardar-btn" type="button" class="btn btn-primary">Guardar</button>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="confirmar" tabindex="-1" role="dialog"
    aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal"
            aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
          <h4 class="modal-title" id="myModalLabel">¿Borrar?</h4>
        </div>
        <div class="modal-body">
          ¿Esta seguro de Borrar el Bloque y todos sus elementos?
        </div>
        <div class="modal-footer">
          <button id="confirmar-guardar-btn" type="button" class="btn btn-primary" >Si</button>
          <button type="button" class="btn btn-danger" data-dismiss="modal">No</button>
        </div>
      </div>
    </div>
  </div>

<script>
js_data = '<?php echo json_encode($lista);?>';
js_obj_data = JSON.parse(js_data);
</script>
<style>
  label{text-align: right;}
</style>