<script>
$(function() {
  jQuery.fn.resetear = function(){
    $(this).each(function(){this.reset();});
  };
});

function nuevo(){
  $('#formulario').resetear();
  $('#modal').appendTo("body").modal('show');
  $( "#guardar-btn").unbind( "click" );
  $( "#guardar-btn" ).bind( "click", function() {
      guardar_nuevo();
  });
}

function guardar_nuevo(){
  
    var datos=$('#formulario').serializeArray();    
    $.ajax({
        type: "POST",
        url: '<?php echo site_url('inventario/compra/nuevo');?>',
        data: datos,
        success: function(response){ 
          $('#nuevo').modal('hide');          
          location.reload();
          alert('Guardado con exito');
        },
        error: function(){alert('Formulario con errores al crear');}
    });   
   
}

function editar(id){
  $('#formulario').resetear();
  datos = buscar(id)[0];  
  populate_form(datos);  
  $('#modal').modal('show');
  $( "#guardar-btn").unbind( "click" );
  $( "#guardar-btn" ).bind( "click", function() {
      guardar_editar(id);
  });
}

function guardar_editar(id){
  
      $.ajax({
            type: "POST",
            url: '<?php echo site_url('inventario/compra/editar/');?>'+id,
            data: $('#formulario').serialize(),
            success: function(response){ 
              $('#modal').modal('hide');
              location.reload();
              alert('Guardado con exito');
            },
            error: function(){alert('Formulario con errores al editar');}
        });
     
}

function buscar(id) {
  return js_obj_data.filter(
          function(data){return data.id == id}
      );
} 

function borrar(id){  
  console.log("borrando");
  $('#confirmar').appendTo("body").modal('show');
  $( "#confirmar-guardar-btn").unbind( "click" );
  $( "#confirmar-guardar-btn" ).bind( "click", function() {
      guardar_borrar(id);
  });
}

function guardar_borrar(id){
  $.ajax({
        type: "POST",
        url: '<?php echo site_url('inventario/compra/borrar/');?>'+id,        
        success: function(response){ 
          $('#confirmar').modal('hide');
          alert('Borrado con exito');
          location.reload();},
        error: function(){alert('Formulario con errores al Borrar');}
    });   
}

function activar(id){  
  $.ajax({
        type: "POST",
        url: '<?php echo site_url('inventario/compra/activar/');?>'+id,       
        success: function(response){ location.reload(); alert('Activado con exito');},
        error: function(){alert('Formulario con errores al Activar');}
    });
}

function populate_form(datos){
  $.each(datos, function(name, val){
      var $el = $('[name="'+name+'"]'),
          type = $el.attr('type');
      switch(type){
          case 'checkbox':
              $el.attr('checked', 'checked');
              break;
          case 'radio':
              $el.filter('[value="'+val+'"]').attr('checked', 'checked');
              break;          
          default:
              $el.val(val);
      }
  });
}
</script>